# Image editor

Application written for Biometry course in Warsaw university of technology.

Includes various algorithms working on picture as a whole, skeletonization algorithms (KMM, K3M) and detecting iris.