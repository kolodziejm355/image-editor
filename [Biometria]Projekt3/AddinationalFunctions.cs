﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _Biometria_Projekt1
{
    public struct HistogramColors
    {
        public int[] red;
        public int[] green;
        public int[] blue;
    }


    public static class AddinationalFunctions
    {

        public static Dictionary<string, int[,]> functionFilters = new Dictionary<string, int[,]>()
        {
            {
                "LowPass1",
                new int[,]
                {
                    {1,1,1},
                    {1,0,1},
                    {1,1,1},
                }
            },
            {
                "LowPass2",
                new int[,]
                {
                    {1,1,1},
                    {1,1,1},
                    {1,1,1},
                }
            },
            {
                "LowPass3",
                new int[,]
                {
                    {1,1,1},
                    {1,2,1},
                    {1,1,1},
                }
            },
            {
                "LowPass4",
                new int[,]
                {
                    {1,1,1},
                    {1,4,1},
                    {1,1,1},
                }
            },
            {
                "Gauss1",
                new int[,]
                {
                    {1,2,1},
                    {2,4,2},
                    {1,2,1},
                }
            },
            {
                "Gauss2",
                new int[,]
                {
                    {1,3,1},
                    {3,9,3},
                    {1,3,1},
                }
            },
            {
                "GaussianBlur1",
                new int[,]
                {
                    {0,1,0},
                    {1,4,1},
                    {0,1,0},
                }  
            },

            {
                "GaussianBlur2",
                new int[,]
                {
                    {0,1,2,1,0},
                    {1,4,8,4,1},
                    {2,8,16,8,2},
                    {1,4,8,4,1},
                    {0,1,2,1,0},
                }
            },
            {
                "HP",
                new int[,]
                {
                    {0,-1,0},
                    {-1,5,-1},
                    {0,-1,0},
                }
            },
            {
                "MeanRemoval",
                new int[,]
                {
                    {-1,-1,-1},
                    {-1, 9,-1},
                    {-1,-1,-1},
                }
            },
            {
                "EdgeDetectionHorizontal",
                new int[,]
                {
                    {0,0,0},
                    {-1,1,0},
                    {0,0,0},
                }
            },
            {
                "EdgeDetectionVertical",
                new int[,]
                {
                    {0,-1,0},
                    {0,1,0},
                    {0,0,0},
                }
            },
            {
                "EdgeDetectionDiagonal",
                new int[,]
                {
                    {-1,0,0},
                    {0,1,0},
                    {0,0,0},
                }
            },
            {
                "EdgeDetectionLaplace1",
                new int[,]
                {
                    {0,-1,0},
                    {-1,4,-1},
                    {0,-1,0},
                }
            },
            {
                "EdgeDetectionLaplace2",
                new int[,]
                {
                    {-1,-1,-1},
                    {-1,8,-1},
                    {-1,-1,-1},
                }
            },
            {
                "SculpingE",
                new int[,]
                {
                    {-1,0,1},
                    {-1,1,1},
                    {-1,0,1},
                }
            },
            {
                "SculpingSE1",
                new int[,]
                {
                    {-1,-1,0},
                    {-1,1,1},
                    {0,1,1},
                }
            },
            {
                "SculpingSE2",
                new int[,]
                {
                    {0,0,0},
                    {0,3,1},
                    {0,1,1},
                }
            },
            {
                "SculpingS",
                new int[,]
                {
                    {-1,-1,-1},
                    {0,1,0},
                    {1,1,1},
                }
            },
            {
                "Roberts1",
                new int[,]
                {
                    {1,0},
                    {0,-1},
                }
            },
            {
                "Roberts2",
                new int[,]
                {
                    {0,1},
                    {-1,0},
                }
            },
            {
                "Roberts3",
                new int[,]
                {
                    {1,-1},
                    {0,0},
                }
            },
            {
                "Roberts4",
                new int[,]
                {
                    {1,0},
                    {-1,0},
                }
            },
        };


        public delegate Color ColorOperation(Color c, double arg);

        public static void OperateOnPixels(Bitmap bmp, ColorOperation op, int arg)
        {
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte red = rgbValues[counter + 2];
                byte green = rgbValues[counter + 1];
                byte blue = rgbValues[counter + 0];

                Color col = Color.FromArgb(red, green, blue);

                Color changed = op(col, arg);
                rgbValues[counter + 3] = 255;
                rgbValues[counter + 2] = changed.R;
                rgbValues[counter + 1] = changed.G;
                rgbValues[counter + 0] = changed.B;
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);
        }

        public static Bitmap Contrast2(Bitmap bmp, double alfa, HistogramColors colors)
        {
            if (alfa <= 0)
            {
                return bmp;
            }

            Bitmap ret = new Bitmap(bmp);



            int maxRed = 0;
            int maxGreen = 0;
            int maxBlue = 0;
            for (int i = 255; i >0; i--)
            {
                if (maxRed < i && colors.red[i] > 0) { maxRed = i; }
                if (maxGreen < i && colors.green[i] > 0) { maxGreen = i; }
                if (maxBlue < i && colors.blue[i] > 0) { maxBlue = i; }
            }

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);


            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte red = rgbValues[counter + 2];
                byte green = rgbValues[counter + 1];
                byte blue = rgbValues[counter + 0];
                
                rgbValues[counter + 3] = 255;
                rgbValues[counter + 2] = (byte) ClampInt((int)Math.Pow((255 * Math.Pow(red / (double) maxRed, alfa)),alfa),0,255);
                rgbValues[counter + 1] = (byte)ClampInt((int)Math.Pow(255 * Math.Pow(green / (double)maxGreen, alfa),alfa), 0, 255);
                rgbValues[counter + 0] = (byte)ClampInt((int)Math.Pow(255 * Math.Pow(blue / (double)maxBlue, alfa),alfa), 0, 255);
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }

        public static Point GetPointFromIndex(int index, int width, int height)
        {
            //because single color is 4 values in array
            width *= 4;

            int currentX = index % width;
            int currentY = index / width;
            return new Point(currentX / 4, currentY);
        }

        public static int GetIndexFromPoint(Point p, int width, int height)
        {
            //because single color is 4 values in array
            width *= 4;
            return p.Y * width + p.X * 4;
        }

        public static int GetIndexAtOffset(int x, int y, int index, int width, int height)
        {
            //because single color is 4 values in array
            x *= 4;
            width *= 4;

            int currentX = index % width;
            int currentY = index / width;

            int newX = currentX + x;
            int newY = currentY + y;

            newX = ClampInt(newX, 0, width - 4);
            newY = ClampInt(newY, 0, height - 1);

            int newIndex = newY * width + newX;
            return newIndex;
        }

        public static int GetIndexAtOffsetMorphologic(int x, int y, int index, int width, int height)
        {
            //because single color is 4 values in array
            x *= 4;
            width *= 4;

            int currentX = index % width;
            int currentY = index / width;

            int newX = currentX + x;
            int newY = currentY + y;

            if (newX < 0 || newX > width - 4)
            {
                return -1;
            }
            if (newY < 0 || newY > height - 1)
            {
                return -1;
            }
         
            int newIndex = newY * width + newX;
            return newIndex;
        }


        public static Bitmap RobertCrossFilter(Bitmap bmp,bool variant)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            
            int dividend = 1;
            int width=  2;
            int height = 2;
            int middleX = 0;
            int middleY = 0;
            int[,] Roberts1 = AddinationalFunctions.functionFilters["Roberts1"];
            int[,] Roberts2 = AddinationalFunctions.functionFilters["Roberts2"];
            if (variant)
            {
                Roberts1 = AddinationalFunctions.functionFilters["Roberts3"];
                Roberts2 = AddinationalFunctions.functionFilters["Roberts4"];
            }

            for (int counter = 0 ; counter < rgbValues.Length; counter += 4)
            {
                int newR = 0;
                int newG = 0;
                int newB = 0;
                int newR1 = 0;
                int newG1 = 0;
                int newB1 = 0;
                int newR2 = 0;
                int newG2 = 0;
                int newB2 = 0;

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        int offsetX = x - middleX;
                        int offsetY = y - middleY;
                        int index = GetIndexAtOffset(offsetX, offsetY, counter, ret.Width, ret.Height);

                        int rOffsetWeighted1 = rgbValues[index + 2] * Roberts1[y, x];
                        int gOffsetWeighted1 = rgbValues[index + 1] * Roberts1[y, x];
                        int bOffsetWeighted1 = rgbValues[index + 0] * Roberts1[y, x];

                        int rOffsetWeighted2 = rgbValues[index + 2] * Roberts2[y, x];
                        int gOffsetWeighted2 = rgbValues[index + 1] * Roberts2[y, x];
                        int bOffsetWeighted2 = rgbValues[index + 0] * Roberts2[y, x];

                        newR1 += rOffsetWeighted1;
                        newG1 += gOffsetWeighted1;
                        newB1 += bOffsetWeighted1;

                        newR2 += rOffsetWeighted2;
                        newG2 += gOffsetWeighted2;
                        newB2 += bOffsetWeighted2;
                    }
                }

                newR = (byte)Math.Sqrt(newR1 * newR1 + newR2 * newR2);
                newG = (byte)Math.Sqrt(newG1 * newG1 + newG2 * newG2);
                newB = (byte)Math.Sqrt(newB1 * newB1 + newB2 * newB2);

                if (dividend == 0)
                    dividend = 1;

                newR = ClampInt(newR / dividend, 0, 255);
                newG = ClampInt(newG / dividend, 0, 255);
                newB = ClampInt(newB / dividend, 0, 255);

                newRgbValues[counter + 3] = 255;
                newRgbValues[counter + 2] = (byte)newR;
                newRgbValues[counter + 1] = (byte)newG;
                newRgbValues[counter + 0] = (byte)newB;
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }
        public static Bitmap MatrixFilter(Bitmap bmp, int[,] matrix)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);


            int dividend = 0;
            foreach (var a in matrix) { dividend += a; }


            int width = matrix.GetLength(0);
            int height = matrix.GetLength(1);

            int middleX = (width - 1) / 2;
            int middleY = (height - 1) / 2;

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                int newR = 0;
                int newG = 0;
                int newB = 0;

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        int offsetX = x - middleX;
                        int offsetY = y - middleY;
                        int index = GetIndexAtOffset(offsetX, offsetY, counter, ret.Width, ret.Height);

                        int rOffsetWeighted = rgbValues[index + 2] * matrix[y, x];
                        int gOffsetWeighted = rgbValues[index + 1] * matrix[y, x];
                        int bOffsetWeighted = rgbValues[index + 0] * matrix[y, x];

                        newR += rOffsetWeighted;
                        newG += gOffsetWeighted;
                        newB += bOffsetWeighted;
                    }
                }

                if (dividend == 0)
                    dividend = 1;

                newR = ClampInt(newR / dividend, 0, 255);
                newG = ClampInt(newG / dividend, 0, 255);
                newB = ClampInt(newB / dividend, 0, 255);

                newRgbValues[counter + 3] = 255;
                newRgbValues[counter + 2] = (byte)newR;
                newRgbValues[counter + 1] = (byte)newG;
                newRgbValues[counter + 0] = (byte)newB;
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }

        public static Bitmap CreateHorizontalProjection(Bitmap bmp)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            int[] pixelNumbers = new int[bmp.Height];

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < 4 * bmp.Width; x+=4)
                {
                    int index = y * bmp.Width * 4 + x;
                    if (index >= rgbValues.Length)
                    {
                        break;
                    }

                    if (rgbValues[index + 2] > 127)
                        pixelNumbers[y]++;
                }
            }

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < 4 * bmp.Width; x += 4)
                {
                    int index = y * bmp.Width * 4 + x;
                    if (index >= rgbValues.Length)
                    {
                        break;
                    }

                    if (pixelNumbers[y] > 0)
                    {
                        rgbValues[index + 3] = 255;
                        rgbValues[index + 2] = 255;
                        rgbValues[index + 1] = 255;
                        rgbValues[index + 0] = 255;
                        pixelNumbers[y]--;
                    }
                    else
                    {
                        rgbValues[index + 3] = 255;
                        rgbValues[index + 2] = 0;
                        rgbValues[index + 1] = 0;
                        rgbValues[index + 0] = 0;
                    }
                }
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }

        public static int[] CreateHorizontalProjectionArray(Bitmap bmp)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            int[] pixelNumbers = new int[bmp.Height];

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < 4 * bmp.Width; x += 4)
                {
                    int index = y * bmp.Width * 4 + x;
                    if (index >= rgbValues.Length)
                    {
                        break;
                    }

                    if (rgbValues[index + 2] < 127)
                        pixelNumbers[y]++;
                }
            }

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return pixelNumbers;
        }

        public static Bitmap CreateVerticalProjection(Bitmap bmp)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            int[] pixelNumbers = new int[bmp.Width];

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < 4 * bmp.Width; x += 4)
                {
                    int index = y * bmp.Width * 4 + x;
                    if (index >= rgbValues.Length)
                    {
                        break;
                    }

                    if (rgbValues[index + 2] > 127)
                        pixelNumbers[x/4]++;
                }
            }

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < 4 * bmp.Width; x += 4)
                {
                    int index = y * bmp.Width * 4 + x;
                    if (index >= rgbValues.Length)
                    {
                        break;
                    }

                    if (pixelNumbers[x/4] > 0)
                    {
                        rgbValues[index + 3] = 255;
                        rgbValues[index + 2] = 255;
                        rgbValues[index + 1] = 255;
                        rgbValues[index + 0] = 255;
                        pixelNumbers[x / 4]--;
                    }
                    else
                    {
                        rgbValues[index + 3] = 255;
                        rgbValues[index + 2] = 0;
                        rgbValues[index + 1] = 0;
                        rgbValues[index + 0] = 0;
                    }
                }
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }

        public static int[] CreateVerticalProjectionArray(Bitmap bmp)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            int[] pixelNumbers = new int[bmp.Width];

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < 4 * bmp.Width; x += 4)
                {
                    int index = y * bmp.Width * 4 + x;
                    if (index >= rgbValues.Length)
                    {
                        break;
                    }

                    if (rgbValues[index + 2] < 127)
                        pixelNumbers[x / 4]++;
                }
            }

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return pixelNumbers;
        }

        public static Bitmap StretchHistogram(Bitmap bmp, bool red, bool green, bool blue)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            byte rMin = byte.MaxValue;
            byte gMin = byte.MaxValue;
            byte bMin = byte.MaxValue;
            byte rMax = byte.MinValue;
            byte gMax = byte.MinValue;
            byte bMax = byte.MinValue;
            
            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte r = rgbValues[counter + 2];
                byte g = rgbValues[counter + 1];
                byte b = rgbValues[counter + 0];

                if (rMin > r)
                    rMin = r;
                if (gMin > g)
                    gMin = g;
                if (bMin > b)
                    bMin = b;

                if (rMax < r)
                    rMax = r;
                if (gMax < g)
                    gMax = g;
                if (bMax < b)
                    bMax = b;
                
                Color col = Color.FromArgb(r, g, b);
            }

            if (rMax - rMin == 255 && gMax - gMin == 255 && bMax - bMin == 255)
            {
                // Unlock the bits.
                ret.UnlockBits(bmpData);
                return bmp;
            }

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte r = rgbValues[counter + 2];
                byte g = rgbValues[counter + 1];
                byte b = rgbValues[counter + 0];

                if (red && rMax - rMin != 0)
                {
                    r = (byte)(255 * (r - rMin) / (rMax - rMin));
                }

                if (green && gMax - gMin != 0)
                {
                    g = (byte)(255 * (g - gMin) / (gMax - gMin));
                }

                if (blue && bMax - bMin != 0)
                {
                    b = (byte)(255 * (b - bMin) / (bMax - bMin) );
                }

                rgbValues[counter + 3] = 255;
                rgbValues[counter + 2] = r;
                rgbValues[counter + 1] = g;
                rgbValues[counter + 0] = b;
            }
            
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }

        public static Bitmap EqualizeHistogram(Bitmap bmp, HistogramColors clr)
        {
            Bitmap ret = new Bitmap(bmp);

            BitmapData bmpData = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;


            //image must be grayscale
            int[] cdf = new int[256];

            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    cdf[i] += clr.red[j];
                }
            }
            int[] h = new int[256];
            for (int i = 0; i < 256; i++)
            {
                h[i] = (int)((cdf[i] - 1) / (double)(bmp.Width * bmp.Height - 1) * 255);
            }

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * ret.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte r = rgbValues[counter + 2];

                r = (byte)h[r];

                rgbValues[counter + 2] = r;
                rgbValues[counter + 1] = r;
                rgbValues[counter + 0] = r;
            }
            
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            ret.UnlockBits(bmpData);

            return ret;
        }

        public static HistogramColors GetHistogramColors(Bitmap bmp)
        {
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), 
                ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);


            HistogramColors colors = new HistogramColors
            {
                red = new int[256],
                green = new int[256],
                blue = new int[256]
            };
            
            
            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte red = rgbValues[counter + 2];
                byte green = rgbValues[counter + 1];
                byte blue = rgbValues[counter + 0];

                colors.red[red]++;
                colors.green[green]++;
                colors.blue[blue]++;
            }

            // Unlock the bits.
            bmp.UnlockBits(bmpData);

            return colors;
        }

        public static int ClampInt(int val, int min, int max)
        {
            return val < min ? min : (val > max ? max : val);
        }



        public static int GetThreshold(Bitmap bmp)
        {
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            double threshold = 0;

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                byte value = rgbValues[counter + 2];
                threshold += value / (double)(bmp.Height * bmp.Width);
            }

            // Unlock the bits.
            bmp.UnlockBits(bmpData);

            return (int)Math.Round(threshold);
        }

        public static Dictionary<int, int> GetLargestNumbers(int[] array, float percentage)
        {
            Dictionary<int, int> ret = new Dictionary<int, int>();
            for (int i = 0; i < array.Length; i++)
            {
                ret.Add(i,array[i]);
            }

            ret = ret.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            int numberOfElems = (int)(percentage * ret.Count);

            ret = ret.Take(numberOfElems).ToDictionary(k => k.Key, k => k.Value);

            return ret;
        }

        public static int WeightedAverageForProjection(Dictionary<int, int> projection)
        {
            ulong sum = 0;
            ulong weight = 0;
            foreach (var pair in projection)
            {
                sum += (ulong)(pair.Key * pair.Value);
                weight += (ulong) pair.Value;
            }

            return (int) (sum / weight);
        }

        public static Tuple<Point, int> GetPupilMiddleAndRadius(Bitmap bmp, float percentage = 0.6f)
        {
            int[] v = CreateVerticalProjectionArray(bmp);
            int[] h = CreateHorizontalProjectionArray(bmp);

            var vDic = GetLargestNumbers(v, percentage);
            var hDic = GetLargestNumbers(h, percentage);

            Point p = new Point()
            {
                Y = WeightedAverageForProjection(hDic),
                X = WeightedAverageForProjection(vDic)
            };

            int radius1=bmp.Width, radius2 = bmp.Width, radius3 = bmp.Width, radius4 = bmp.Width;
            for (int x = p.X; x < v.Length; x++)
            {
                if (v[x] == 0)
                {
                    radius1 = x - 1 - p.X;
                    break;
                }
            }
            for (int x = p.X; x >= 0; x--)
            {
                if (v[x] == 0)
                {
                    radius2 = p.X - (x + 1);
                    break;
                }
            }
            for (int y = p.Y; y < h.Length; y++)
            {
                if (h[y] == 0)
                {
                    radius3 = y - 1 - p.Y;
                    break;
                }
            }
            for (int y = p.Y; y >= 0; y--)
            {
                if (h[y] == 0)
                {
                    radius4 =  p.Y - (y + 1);
                    break;
                }
            }
            return new Tuple<Point, int>(p, (radius1 + radius2 + radius3 + radius4)/4);
        }

        public static double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }

        //for binarized image
        public static int GetRayLength(byte[] rgbValues, Point middle, double angle, int width, int height)
        {
            double x = middle.X;
            double y = middle.Y;
            int index = GetIndexAtOffset((int)x, (int)y, 0, width, height);

            double xStep = Math.Cos(ConvertToRadians(angle));
            double yStep = Math.Sin(ConvertToRadians(angle));
            double length = 0;

            while (rgbValues[index] == 0)
            {
                length += Math.Sqrt(Math.Pow(xStep,2) + Math.Pow(yStep,2));
                x += xStep;
                y += yStep;
                int X = (int) x;
                int Y = (int) y;
                if (X < 0 || Y < 0 || X >= width || Y >= height)
                    return (int)length;
                index = GetIndexAtOffset(X, Y, 0, width, height);
            }
            
            return (int)length;
        }

        public static int[] GetRayLengths(Bitmap bmp, Point middle)
        {
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            int[] ret = new int[6];
            ret[0] = GetRayLength(rgbValues, middle, 0, bmp.Width, bmp.Height);
            ret[1] = GetRayLength(rgbValues, middle, 20, bmp.Width, bmp.Height);
            ret[2] = GetRayLength(rgbValues, middle, -20, bmp.Width, bmp.Height);
            ret[3] = GetRayLength(rgbValues, middle, 180, bmp.Width, bmp.Height);
            ret[4] = GetRayLength(rgbValues, middle, 200, bmp.Width, bmp.Height);
            ret[5] = GetRayLength(rgbValues, middle, 160, bmp.Width, bmp.Height);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);
            return ret;
        }

        public static int GetIrisRadius(Bitmap bmp, Point middle)
        {
            int[] lengths = GetRayLengths(bmp, middle);
            int[] distanceSums = new int[lengths.Length];

            for (int i = 0; i < distanceSums.Length - 1; i++)
            {
                for (int j = i + 1; j < distanceSums.Length; j++)
                {
                    distanceSums[i] += Math.Abs(lengths[i] - lengths[j]);
                    distanceSums[j] += Math.Abs(lengths[i] - lengths[j]);
                }
            }
            Dictionary<int, int> smallestDistanceSums = new Dictionary<int, int>();
            for (int i = 0; i < distanceSums.Length; i++)
            {
                smallestDistanceSums.Add(i, distanceSums[i]);
            }

            int numberOfElems = 3;
            smallestDistanceSums = smallestDistanceSums.OrderBy(x => x.Value).Take(numberOfElems).ToDictionary(x => x.Key, x => x.Value);
            double length = 0;
            foreach (var key in smallestDistanceSums.Keys)
            {
                length += lengths[key];
            }

            return (int)(length / numberOfElems);
        }


        ///////////////////////////////////////////
        // MORPHOLOGIC OPERATIONS
        //

        public static MorphologicMatrix GetMorphologicMatrixSquare(int a)
        {
            bool[,] mat = new bool[a,a];
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    mat[i, j] = true;
                }
            }
            return new MorphologicMatrix(mat, a/2, a/2 );
        }

        public static Bitmap Dilation(Bitmap bmp, MorphologicMatrix mat)
        {
            Bitmap copy = (Bitmap)bmp.Clone();
            BitmapData bmpData = copy.LockBits(new Rectangle(0, 0, copy.Width, copy.Height), ImageLockMode.ReadWrite,
                PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * copy.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            System.Runtime.InteropServices.Marshal.Copy(ptr, newRgbValues, 0, bytes);

            int width = mat.width;
            int height = mat.height;

            int middleX = mat.impX;
            int middleY = mat.impY;

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                bool works = true;

                if (mat[mat.impY, mat.impX])
                {
                    if (rgbValues[counter + 2] != 0)
                    {
                        works = false;
                    }
                }
                if (works)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int y = 0; y < height; y++)
                        {
                            int offsetX = x - middleX;
                            int offsetY = y - middleY;
                            int index = GetIndexAtOffsetMorphologic(offsetX, offsetY, counter, copy.Width, copy.Height);
                            
                            if (index != -1 && mat[y, x])
                            {
                                newRgbValues[index + 3] = 255;//alpha
                                newRgbValues[index + 2] = 0;
                                newRgbValues[index + 1] = 0;
                                newRgbValues[index + 0] = 0;
                            }
                        }
                    }
                }
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);
            // Unlock the bits.
            copy.UnlockBits(bmpData);
            return copy;
        }

        public static Bitmap Erosion(Bitmap bmp, MorphologicMatrix mat)
        {
            Bitmap copy = (Bitmap)bmp.Clone();
            BitmapData bmpData = copy.LockBits(new Rectangle(0, 0, copy.Width, copy.Height), ImageLockMode.ReadWrite,
                  PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * copy.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            System.Runtime.InteropServices.Marshal.Copy(ptr, newRgbValues, 0, bytes);

            int width = mat.width;
            int height = mat.height;

            int middleX = mat.impX;
            int middleY = mat.impY;

            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                bool works = true;
                for (int x = 0; x < width; x++)
                {
                    if (!works) break;
                    for (int y = 0; y < height; y++)
                    {
                        int offsetX = x - middleX;
                        int offsetY = y - middleY;
                        int index = GetIndexAtOffsetMorphologic(offsetX, offsetY, counter, copy.Width, copy.Height);

                        if (index == -1 || (mat[y, x] && rgbValues[index + 2] == 255))
                        {
                            works = false;
                            break;
                        }
                    }
                }

                if (!works && mat[mat.impY, mat.impX])
                {
                    newRgbValues[counter + 3] = 255;
                    newRgbValues[counter + 2] = 255;
                    newRgbValues[counter + 1] = 255;
                    newRgbValues[counter + 0] = 255;
                }
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);
            // Unlock the bits.
            copy.UnlockBits(bmpData);
            return copy;
        }
        public static Bitmap Opening(Bitmap bmp, MorphologicMatrix mat)
        {
            return Dilation(Erosion(bmp, mat), mat);
        }
        public static Bitmap Closing(Bitmap bmp, MorphologicMatrix mat)
        {
            return Erosion(Dilation(bmp, mat), mat);
        }

        public static Bitmap RemoveNonNeighbours(Bitmap bmp, Point middlePoint)
        {
            bool[,] addedToQueue = new bool[bmp.Height, bmp.Width];

            Bitmap copy = (Bitmap)bmp.Clone();
            BitmapData bmpData = copy.LockBits(new Rectangle(0, 0, copy.Width, copy.Height), ImageLockMode.ReadWrite,
                  PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * copy.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];
            for (int i=0;i<newRgbValues.Length;i++)
            {
                newRgbValues[i] = 255;
            }

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            
            Queue<Point> toVisit = new Queue<Point>();

            toVisit.Enqueue(new Point(middlePoint.X,middlePoint.Y));
            addedToQueue[middlePoint.Y, middlePoint.X] = true;

            while (toVisit.Count > 0)
            {
                Point p = toVisit.Dequeue();
                int index = GetIndexAtOffset(p.X, p.Y, 0, bmp.Width, bmp.Height);
                if (rgbValues[index + 2] == 0)
                {
                    newRgbValues[index + 2] = 0;
                    newRgbValues[index + 1] = 0;
                    newRgbValues[index + 0] = 0;

                    if (p.X - 1 >= 0 && !addedToQueue[p.Y, p.X - 1])
                    {
                        addedToQueue[p.Y, p.X - 1] = true;
                        toVisit.Enqueue(new Point(p.X - 1, p.Y));
                    }
                    if (p.Y - 1 >= 0 && !addedToQueue[p.Y - 1, p.X])
                    {
                        addedToQueue[p.Y - 1, p.X] = true;
                        toVisit.Enqueue(new Point(p.X, p.Y - 1));
                    }
                    if (p.X + 1 < bmp.Width && !addedToQueue[p.Y, p.X + 1])
                    {
                        addedToQueue[p.Y, p.X + 1] = true;
                        toVisit.Enqueue(new Point(p.X + 1, p.Y));
                    }
                    if (p.Y + 1 < bmp.Height && !addedToQueue[p.Y + 1, p.X])
                    {
                        addedToQueue[p.Y + 1, p.X] = true;
                        toVisit.Enqueue(new Point(p.X, p.Y + 1));
                    }
                }
            }
            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);
            // Unlock the bits.
            copy.UnlockBits(bmpData);
            return copy;
        }

        public static int getMask(int[,] Numbers, int x, int y, int width, int height)
        {
            int mask = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (x + i >= 0 && x + i < width && y + j >= 0 && y + j < height)
                    {
                        if (i == -1 && j == -1 && Numbers[y + j, x + i] > 0) mask += 128;
                        if (i == -1 && j == 0 && Numbers[y + j, x + i] > 0) mask += 64;
                        if (i == -1 && j == 1 && Numbers[y + j, x + i] > 0) mask += 32;
                        if (i == 0 && j == -1 && Numbers[y + j, x + i] > 0) mask += 1;
                        if (i == 0 && j == 1 && Numbers[y + j, x + i] > 0) mask += 16;
                        if (i == 1 && j == -1 && Numbers[y + j, x + i] > 0) mask += 2;
                        if (i == 1 && j == 0 && Numbers[y + j, x + i] > 0) mask += 4;
                        if (i == 1 && j == 1 && Numbers[y + j, x + i] > 0) mask += 8;
                    }
                }
            }
            return mask;
        }


        public static Bitmap KMM(Bitmap bmp)
        {
            bool[,] addedToQueue = new bool[bmp.Height, bmp.Width];

            Bitmap copy = (Bitmap)bmp.Clone();
            BitmapData bmpData = copy.LockBits(new Rectangle(0, 0, copy.Width, copy.Height), ImageLockMode.ReadWrite,
                  PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * copy.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];
            for (int i = 0; i < newRgbValues.Length; i++)
            {
                newRgbValues[i] = 255;
            }

            int[,] Numbers = new int[bmp.Height, bmp.Width];


            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);


            //Set numbers
            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                Point p = GetPointFromIndex(counter, copy.Width, copy.Height);
                if (rgbValues[counter + 2] == 0)
                {
                    Numbers[p.Y, p.X] = 1;
                }
            }

            bool changes = true;
            int[] fourNeighbouringMasks =
            {
                3, 6, 7, 12, 14, 15, 24, 28, 30, 48, 56, 60, 96, 112, 120, 129, 131, 135, 192, 193, 195, 224, 225, 240
            };
            int[] deletionMasks =
            {
                3, 5, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29,
                30, 31, 48, 52, 53, 54, 55, 56, 60, 61, 62, 63, 65, 67, 69,
                71, 77, 79, 80, 81, 83, 84, 85, 86, 87, 88, 89, 91, 92, 93,
                94, 95, 97, 99, 101, 103, 109, 111, 112, 113, 115, 116, 117,
                118, 119, 120, 121, 123, 124, 125, 126, 127, 131, 133, 135,
                141, 143, 149, 151, 157, 159, 181, 183, 189, 191, 192, 193,
                195, 197, 199, 205, 207, 208, 209, 211, 212, 213, 214, 215,
                216, 217, 219, 220, 221, 222, 223, 224, 225, 227, 229, 231,
                237, 239, 240, 241, 243, 244, 245, 246, 247, 248, 249, 251,
                252, 253, 254, 255
            };
            while (changes)
            {
                changes = false;
                //change to 2 and 3
                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        if (Numbers[y, x] > 0)
                        {

                            int mask = getMask(Numbers, x, y, bmp.Width, bmp.Height);

                            if (fourNeighbouringMasks.Contains(mask))
                            {
                                Numbers[y, x] = 4;
                            }
                            else if ((x + 1 < bmp.Width && Numbers[y, x + 1] == 0)
                                     || (x - 1 >= 0 && Numbers[y, x - 1] == 0)
                                     || (y + 1 < bmp.Height && Numbers[y + 1, x] == 0)
                                     || (y - 1 >= 0 && Numbers[y - 1, x] == 0))
                            {
                                Numbers[y, x] = 2;
                            }
                            else if ((x + 1 < bmp.Width && y + 1 < bmp.Height && Numbers[y + 1, x + 1] == 0)
                                     || (x - 1 >= 0 && y + 1 < bmp.Height && Numbers[y + 1, x - 1] == 0)
                                     || (x + 1 < bmp.Width && y - 1 >= 0 && Numbers[y - 1, x + 1] == 0)
                                     || (x - 1 >= 0 && y - 1 >= 0 && Numbers[y - 1, x - 1] == 0))
                            {
                                Numbers[y, x] = 3;
                            }
                        }
                    }
                }

                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        if (Numbers[y, x] == 4)
                        {
                            changes = true;
                            Numbers[y, x] = 0;
                        }
                    }
                }

                for (int N = 2; N <= 3; N++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        for (int y = 0; y < bmp.Height; y++)
                        {
                            if (Numbers[y, x] == N)
                            {
                                int mask = getMask(Numbers,x,y, bmp.Width, bmp.Height);
                                
                                if (deletionMasks.Contains(mask))
                                {
                                    changes = true;
                                    Numbers[y, x] = 0;
                                }
                                else
                                {
                                    Numbers[y, x] = 1;
                                }
                            }
                        }
                    }
                }
            }


            //Set pixels using numbers
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    int index = GetIndexFromPoint(new Point(x,y), copy.Width, copy.Height);
                    
                    if (Numbers[y,x] > 0)
                    {
                        newRgbValues[index + 2] = 0;
                        newRgbValues[index + 1] = 0;
                        newRgbValues[index + 0] = 0;
                    }
                }
            }
        

            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);
            // Unlock the bits.
            copy.UnlockBits(bmpData);
            return copy;
        }

        public static Bitmap K3M(Bitmap bmp)
        {
            bool[,] addedToQueue = new bool[bmp.Height, bmp.Width];

            Bitmap copy = (Bitmap)bmp.Clone();
            BitmapData bmpData = copy.LockBits(new Rectangle(0, 0, copy.Width, copy.Height), ImageLockMode.ReadWrite,
                  PixelFormat.Format32bppPArgb);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * copy.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] newRgbValues = new byte[bytes];
            for (int i = 0; i < newRgbValues.Length; i++)
            {
                newRgbValues[i] = 255;
            }

            int[,] Numbers = new int[bmp.Height, bmp.Width];


            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);


            //Set numbers
            for (int counter = 0; counter < rgbValues.Length; counter += 4)
            {
                Point p = GetPointFromIndex(counter, copy.Width, copy.Height);
                if (rgbValues[counter + 2] == 0)
                {
                    Numbers[p.Y, p.X] = 1;
                }
            }

            bool changes = true;
            
            int[] A0 =
            {
                3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56, 60,
                62, 63, 96, 112, 120, 124, 126, 127, 129, 131, 135,
                143, 159, 191, 192, 193, 195, 199, 207, 223, 224,
                225, 227, 231, 239, 240, 241, 243, 247, 248, 249,
                251, 252, 253, 254
            };
            int[][] Ai =
                new int[][]
                {
                    new int[]
                    {
                        7, 14, 28, 56, 112, 131, 193, 224
                    },

                    new int[]
                    {
                        7, 14, 15, 28, 30, 56, 60, 112, 120, 131, 135,
                        193, 195, 224, 225, 240
                    },
                    new int[]
                    {
                        7, 14, 15, 28, 30, 31, 56, 60, 62, 112, 120,
                        124, 131, 135, 143, 193, 195, 199, 224, 225, 227,
                        240, 241, 248
                    },
                    new int[]
                    {
                        7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
                        124, 126, 131, 135, 143, 159, 193, 195, 199, 207,
                        224, 225, 227, 231, 240, 241, 243, 248, 249, 252
                    },
                    new int[]
                    {
                        7, 14, 15, 28, 30, 31, 56, 60, 62, 63, 112, 120,
                        124, 126, 131, 135, 143, 159, 191, 193, 195, 199,
                        207, 224, 225, 227, 231, 239, 240, 241, 243, 248,
                        249, 251, 252, 254
                    }
                };
            int[] A1pix =
            {
                3, 6, 7, 12, 14, 15, 24, 28, 30, 31, 48, 56,
                60, 62, 63, 96, 112, 120, 124, 126, 127, 129, 131,
                135, 143, 159, 191, 192, 193, 195, 199, 207, 223,
                224, 225, 227, 231, 239, 240, 241, 243, 247, 248,
                249, 251, 252, 253, 254
            };
            while (changes)
            {
                changes = false;
                //PHASE 0
                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        if (Numbers[y, x] > 0)
                        {
                            int mask = getMask(Numbers, x, y, bmp.Width, bmp.Height);

                            if (A0.Contains(mask))
                            {
                                Numbers[y, x] = 2; //border
                            }
                        }
                    }
                }

                //PHASE 1-5
                for (int i = 0; i < 5; i++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        for (int y = 0; y < bmp.Height; y++)
                        {
                            if (Numbers[y, x] == 2)
                            {
                                int mask = getMask(Numbers, x, y, bmp.Width, bmp.Height);

                                if (Ai[i].Contains(mask))
                                {
                                    changes = true;
                                    Numbers[y, x] = 0; //background
                                }
                            }
                        }
                    }
                }
                //PHASE 6
                for (int x = 0; x < bmp.Width; x++)
                {
                    for (int y = 0; y < bmp.Height; y++)
                    {
                        if (Numbers[y, x] == 2)
                        {
                            Numbers[y, x] = 1;
                        }
                    }
                }
            }
            //PHASE 7
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    if (Numbers[y, x] == 1)
                    {
                        int mask = getMask(Numbers, x, y, bmp.Width, bmp.Height);
                        if (A1pix.Contains(mask))
                        {
                            Numbers[y, x] = 0;
                        }
                    }
                }
            }


            //Set pixels using numbers
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    int index = GetIndexFromPoint(new Point(x, y), copy.Width, copy.Height);

                    if (Numbers[y, x] > 0)
                    {
                        newRgbValues[index + 2] = 0;
                        newRgbValues[index + 1] = 0;
                        newRgbValues[index + 0] = 0;
                    }
                }
            }


            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(newRgbValues, 0, ptr, bytes);
            // Unlock the bits.
            copy.UnlockBits(bmpData);
            return copy;
        }
    }
}
