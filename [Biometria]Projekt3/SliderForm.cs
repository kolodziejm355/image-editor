﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _Biometria_Projekt1
{
    public partial class SliderForm : Form
    {
        public string title;

        public SliderForm()
        {
            InitializeComponent();
        }

        private void OK_button_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Cancel_button_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            LabelAndValue.Text = title + ": " + trackBar.Value;
        }

        private void SliderForm_Load(object sender, EventArgs e)
        {
            LabelAndValue.Text = title + ": " + trackBar.Value;
        }
    }
}
