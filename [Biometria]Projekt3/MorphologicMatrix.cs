﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria_Projekt1
{
    public class MorphologicMatrix
    {
        private bool[,] matrix;
        public int impX;
        public int impY;
        public int width;
        public int height;

        public bool this[int key,int key2]
        {
            get
            {
                return matrix[key,key2];
            }
        }

        public MorphologicMatrix(bool[,] mat, int importantX, int importantY)
        {
            matrix = mat;
            impX = importantX;
            impY = importantY;
            width = mat.GetLength(1);
            height = mat.GetLength(0);
        }
    }
}
