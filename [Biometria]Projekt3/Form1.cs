﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace _Biometria_Projekt1
{
    public partial class AppForm : Form
    {
        private HistogramColors currentHistogramColors;
        
        public AppForm()
        {
            InitializeComponent();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// LOAD AND SAVE
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    Picture.Image = new Bitmap(dlg.FileName);
                    RefreshHistograms();
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "Images|*.png;*.bmp;*.jpg";
                ImageFormat format = ImageFormat.Png;
                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string ext = System.IO.Path.GetExtension(sfd.FileName);
                    switch (ext)
                    {
                        case ".jpg":
                            format = ImageFormat.Jpeg;
                            break;
                        case ".bmp":
                            format = ImageFormat.Bmp;
                            break;
                    }
                    Picture.Image.Save(sfd.FileName, format);
                }
            }
        }


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// OperationsClicks
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void changeBrightnessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            SliderForm sliderForm = new SliderForm();
            sliderForm.Owner = this;
            sliderForm.StartPosition = FormStartPosition.CenterParent;

            sliderForm.title = "Brightness";
            sliderForm.trackBar.Minimum = -255;
            sliderForm.trackBar.Maximum = 255;
            sliderForm.trackBar.SmallChange = 1;
            sliderForm.trackBar.ValueChanged += BrightnessChangeEvent;

            sliderForm.Cancel_button.Click += ReloadImageEvent;

            Bitmap bmp = (Bitmap)Picture.Image;
            SavedBitmap = (Bitmap)bmp.Clone();

            sliderForm.ShowDialog(this);
            RefreshHistograms();
        }

        private void changeContrastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            SliderForm sliderForm = new SliderForm();
            sliderForm.Owner = this;
            sliderForm.StartPosition = FormStartPosition.CenterParent;

            sliderForm.title = "Contrast";
            sliderForm.trackBar.Minimum = -255;
            sliderForm.trackBar.Maximum = 255;
            sliderForm.trackBar.SmallChange = 1;
            sliderForm.trackBar.ValueChanged += ContrastChangeEvent;

            sliderForm.Cancel_button.Click += ReloadImageEvent;

            Bitmap bmp = (Bitmap)Picture.Image;
            SavedBitmap = (Bitmap)bmp.Clone();

            sliderForm.ShowDialog(this);
            RefreshHistograms();
        }

        private void contrast2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            SliderForm sliderForm = new SliderForm();
            sliderForm.Owner = this;
            sliderForm.StartPosition = FormStartPosition.CenterParent;

            sliderForm.title = "Contrast";
            sliderForm.trackBar.Minimum = -200;
            sliderForm.trackBar.Maximum = 200;
            sliderForm.trackBar.SmallChange = 1;
            sliderForm.trackBar.ValueChanged += ContrastChange2Event;

            sliderForm.Cancel_button.Click += ReloadImageEvent;

            Bitmap bmp = (Bitmap)Picture.Image;
            SavedBitmap = (Bitmap)bmp.Clone();

            sliderForm.ShowDialog(this);
            RefreshHistograms();
        }

        private void changeToGrayscaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            AddinationalFunctions.OperateOnPixels(bmp,ConvertToGrayscale,0);
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void negationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            AddinationalFunctions.OperateOnPixels(bmp, Negation, 0);
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void thresholdingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            SliderForm sliderForm = new SliderForm();
            sliderForm.Owner = this;
            sliderForm.StartPosition = FormStartPosition.CenterParent;

            sliderForm.title = "Threshold Red";
            sliderForm.trackBar.Minimum = 0;
            sliderForm.trackBar.Maximum = 256;
            sliderForm.trackBar.SmallChange = 1;
            sliderForm.trackBar.ValueChanged += ThresholdChangeEvent;

            sliderForm.Cancel_button.Click += ReloadImageEvent;

            Bitmap bmp = (Bitmap)Picture.Image;
            SavedBitmap = (Bitmap)bmp.Clone();

            sliderForm.ShowDialog(this);
            RefreshHistograms();
        }

        private void histogramEqualizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.EqualizeHistogram(bmp, currentHistogramColors);
            Picture.Image = bmp;
            RefreshHistograms();
        }



        private void histogramStretchingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.StretchHistogram(bmp, true, true, true);
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void HistogramStretchingRedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.StretchHistogram(bmp, true, false, false);
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void HistogramStretchingGreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.StretchHistogram(bmp, false, true, false);
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void HistogramStretchingBlueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.StretchHistogram(bmp, false, false, true);
            Picture.Image = bmp;
            RefreshHistograms();
        }


        private void horizontalProjectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.CreateHorizontalProjection(bmp);
            Picture.Image = bmp;
            RefreshHistograms();

        }

        private void verticalProjectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.CreateVerticalProjection(bmp);
            Picture.Image = bmp;
            RefreshHistograms();
        }




        private void MatrixFiltering(string filter)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.MatrixFilter(bmp, AddinationalFunctions.functionFilters[filter]);
            Picture.Image = bmp;
            RefreshHistograms();
        }
        
        private void gaussianBlur1ToolStripMenuItem_Click(object sender, EventArgs e) {MatrixFiltering("GaussianBlur1");}
        private void gaussianBlur2ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("GaussianBlur2"); }
        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("EdgeDetectionHorizontal"); }
        private void verticalToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("EdgeDetectionVertical"); }
        private void diagonalToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("EdgeDetectionDiagonal"); }
        private void laplace1ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("EdgeDetectionLaplace1"); }
        private void laplace2ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("EdgeDetectionLaplace2"); }
        private void lowPass1ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("LowPass1"); }
        private void lowPass2ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("LowPass2"); }
        private void lowPass3ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("LowPass3"); }
        private void lowPass4ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("LowPass4"); }
        private void hPToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("HP"); }
        private void meanRemovalToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("MeanRemoval"); }
        private void gaussianBlur3ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("Gauss1"); }
        private void gaussianBlur4ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("Gauss2"); }
        private void sculpingEToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("SculpingE"); }
        private void sculpingSToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("SculpingS"); }
        private void sculpingSE1ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("SculpingSE1"); }
        private void sculpingSE2ToolStripMenuItem_Click(object sender, EventArgs e) { MatrixFiltering("SculpingSE2"); }

        private void robertsCrossToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.RobertCrossFilter(bmp, true);
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void robertsCross2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Picture.Image == null)
            {
                return;
            }
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.RobertCrossFilter(bmp, false);
            Picture.Image = bmp;
            RefreshHistograms();
        }
        
        AddinationalFunctions.ColorOperation brightnessChange = (c, a) =>
        {
            int red = AddinationalFunctions.ClampInt(c.R + (int)a, 0, 255);
            int green = AddinationalFunctions.ClampInt(c.G + (int)a, 0, 255);
            int blue = AddinationalFunctions.ClampInt(c.B + (int)a, 0, 255);
            Color ret = Color.FromArgb(red, green, blue);
            return ret;
        };

        private AddinationalFunctions.ColorOperation contrastChange = (c, a) =>
        {
            double factor = (259 * (a + 255)) / (double)(255 * (259 - a));
            int red = AddinationalFunctions.ClampInt((int)(factor * (c.R - 128)) + 128, 0, 255);
            int green = AddinationalFunctions.ClampInt((int)(factor * (c.G - 128)) + 128, 0, 255);
            int blue = AddinationalFunctions.ClampInt((int)(factor * (c.B - 128)) + 128, 0, 255);
            Color ret = Color.FromArgb(red, green, blue);
            return ret;
        };

        private AddinationalFunctions.ColorOperation Negation = (c, a) =>
        {
            int red = 255 - c.R;
            int green = 255 - c.G;
            int blue = 255 - c.B;
            Color ret = Color.FromArgb(red, green, blue);
            return ret;
        };

        private AddinationalFunctions.ColorOperation ConvertToGrayscale = (c, a) =>
        {
            int gray = (int) (c.R * 0.3 + c.G * 0.59 + c.B * 0.11);
            Color ret = Color.FromArgb(gray, gray, gray);
            return ret;
        };

        private AddinationalFunctions.ColorOperation thresholdChange = (c, a) =>
        {
            int val = (c.R >= a ? 255 : 0);
            Color ret = Color.FromArgb(val, val, val);
            return ret;
        };
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Addinational Events
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        private Bitmap SavedBitmap;

        private void ReloadImageEvent(object sender, System.EventArgs e)
        {
            Bitmap bmp = (Bitmap)SavedBitmap.Clone();
            Picture.Image = bmp;
        }

        private void BrightnessChangeEvent(object sender, System.EventArgs e)
        {
            TrackBar t = (TrackBar)sender;
            int value = t.Value;

            Bitmap bmp = (Bitmap)SavedBitmap.Clone();
            AddinationalFunctions.OperateOnPixels(bmp, brightnessChange, value);
            Picture.Image = bmp;
        }

        private void ContrastChangeEvent(object sender, System.EventArgs e)
        {
            TrackBar t = (TrackBar)sender;
            int value = t.Value;

            Bitmap bmp = (Bitmap)SavedBitmap.Clone();
            AddinationalFunctions.OperateOnPixels(bmp, contrastChange, value);
            Picture.Image = bmp;

        }

        private void ContrastChange2Event(object sender, System.EventArgs e)
        {
            TrackBar t = (TrackBar)sender;
            double value = 1 + t.Value * 0.001;

            Bitmap bmp = (Bitmap)SavedBitmap.Clone();
            
            Picture.Image = AddinationalFunctions.Contrast2(bmp, value, currentHistogramColors);

        }

        private void ThresholdChangeEvent(object sender, System.EventArgs e)
        {
            TrackBar t = (TrackBar)sender;
            int value = t.Value;

            Bitmap bmp = (Bitmap)SavedBitmap.Clone();
            AddinationalFunctions.OperateOnPixels(bmp, thresholdChange, value);
            Picture.Image = bmp;
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Histograms
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        private void RefreshHistograms()
        {
            Bitmap bmp = (Bitmap)Picture.Image;
            HistogramColors colors = AddinationalFunctions.GetHistogramColors(bmp);
            currentHistogramColors = colors;

            RefreshHistogram(HistogramRed, "RedSeries", colors.red);
            RefreshHistogram(HistogramGreen, "GreenSeries", colors.green);
            RefreshHistogram(HistogramBlue, "BlueSeries", colors.blue);
        }

        private void RefreshHistogram(Chart histogram, string name, int[] values) 
        {

            if (histogram.Series[name].Points.Count < 256)
            {
                int pointsLeft = 256 - histogram.Series[name].Points.Count;
                for (int i = 0; i < pointsLeft; i++)
                {
                    histogram.Series[name].Points.Add(0);
                }
            }
            int max = 0;
            //Refresh histograms
            for (int i = 0; i < 256; i++)
            {
                int y = values[i];
                histogram.Series[name].Points[i].SetValueXY(i, y);
                if (y > max)
                {
                    max = y;
                }
            }
            histogram.ChartAreas[0].AxisX.Maximum = 258;
            histogram.ChartAreas[0].AxisX.Minimum = -2;
            histogram.ChartAreas[0].AxisY.Maximum = max;
            histogram.Update();
            histogram.Refresh();
        }

        private void getIrisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap bmp = (Bitmap)Picture.Image;
            AddinationalFunctions.OperateOnPixels(bmp, ConvertToGrayscale, 0);
            bmp = AddinationalFunctions.StretchHistogram(bmp, true, true, true);
            
            Bitmap pupil = (Bitmap)bmp.Clone();
            Bitmap iris = (Bitmap)bmp.Clone();

            pupil = AddinationalFunctions.MatrixFilter(pupil, AddinationalFunctions.functionFilters["GaussianBlur2"]);
            iris = AddinationalFunctions.MatrixFilter(iris, AddinationalFunctions.functionFilters["MeanRemoval"]);
            int threshold = AddinationalFunctions.GetThreshold(bmp);

            int PupilThreshold = (int)(threshold / 4.5f);
            int IrisThreshold = (int)(threshold / 1.4f);
            AddinationalFunctions.OperateOnPixels(pupil, thresholdChange, PupilThreshold);
            AddinationalFunctions.OperateOnPixels(iris, thresholdChange, IrisThreshold);
            
            pupil = AddinationalFunctions.Closing(pupil, AddinationalFunctions.GetMorphologicMatrixSquare(5));
            pupil = AddinationalFunctions.RemoveNonNeighbours(pupil, new Point(pupil.Width / 2, pupil.Height / 2));
            pupil = AddinationalFunctions.Opening(pupil, AddinationalFunctions.GetMorphologicMatrixSquare(5));
            pupil = AddinationalFunctions.Closing(pupil, AddinationalFunctions.GetMorphologicMatrixSquare(17));
            
            iris = AddinationalFunctions.Closing(iris, AddinationalFunctions.GetMorphologicMatrixSquare(5));
            iris = AddinationalFunctions.Opening(iris, AddinationalFunctions.GetMorphologicMatrixSquare(9));
            iris = AddinationalFunctions.RemoveNonNeighbours(iris, new Point(iris.Width / 2, iris.Height / 2));
            iris = AddinationalFunctions.Closing(iris, AddinationalFunctions.GetMorphologicMatrixSquare(13));
            
            int pupilRadius, irisRadius;
            Point pupilMiddle, irisMiddle;
            
            Tuple<Point, int> tmp = AddinationalFunctions.GetPupilMiddleAndRadius(pupil);
            pupilMiddle = tmp.Item1;
            pupilRadius = tmp.Item2;

            irisMiddle = pupilMiddle;
            irisRadius = AddinationalFunctions.GetIrisRadius(iris, irisMiddle);

            using (var graphics = Graphics.FromImage(bmp))
            {
                Pen pen = new Pen(Color.FromArgb(255, 255, 140, 0), 2);
                graphics.DrawEllipse(pen, pupilMiddle.X - pupilRadius, pupilMiddle.Y - pupilRadius, 2 * pupilRadius, 2 * pupilRadius);
                graphics.DrawEllipse(pen, irisMiddle.X - irisRadius, irisMiddle.Y - irisRadius, 2 * irisRadius, 2 * irisRadius);
            }
            
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void kMMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.KMM(bmp);
            
            Picture.Image = bmp;
            RefreshHistograms();
        }

        private void k3MToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap bmp = (Bitmap)Picture.Image;
            bmp = AddinationalFunctions.K3M(bmp);

            Picture.Image = bmp;
            RefreshHistograms();
        }
    }
}
