﻿namespace _Biometria_Projekt1
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint13 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint14 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint15 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint16 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint17 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint18 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0D, 5D);
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.Picture = new System.Windows.Forms.PictureBox();
            this.HistogramRed = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.getIrisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeBrightnessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeContrastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contrast2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeToGrayscaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramStretchingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HistogramStretchingRedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HistogramStretchingGreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HistogramStretchingBlueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramEqualizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalProjectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalProjectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.functionFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowPassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowPass1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowPass2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowPass3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lowPass4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gaussianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gaussianBlur1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gaussianBlur2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gaussianBlur3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gaussianBlur4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharpeningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.meanRemovalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgeDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagonalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laplace1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laplace2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.robertsCrossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.robertsCross2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sculpingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sculpingEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sculpingSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sculpingSE1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sculpingSE2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HistogramGreen = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.HistogramBlue = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.kMMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.k3MToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistogramRed)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistogramGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistogramBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Picture
            // 
            this.Picture.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Picture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Picture.Location = new System.Drawing.Point(0, 0);
            this.Picture.Name = "Picture";
            this.Picture.Size = new System.Drawing.Size(665, 546);
            this.Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Picture.TabIndex = 0;
            this.Picture.TabStop = false;
            // 
            // HistogramRed
            // 
            chartArea4.Name = "ChartArea1";
            this.HistogramRed.ChartAreas.Add(chartArea4);
            this.HistogramRed.Dock = System.Windows.Forms.DockStyle.Bottom;
            legend4.Enabled = false;
            legend4.Name = "Legend1";
            this.HistogramRed.Legends.Add(legend4);
            this.HistogramRed.Location = new System.Drawing.Point(0, 27);
            this.HistogramRed.Name = "HistogramRed";
            series4.ChartArea = "ChartArea1";
            series4.Color = System.Drawing.Color.Red;
            series4.Legend = "Legend1";
            series4.Name = "RedSeries";
            series4.Points.Add(dataPoint10);
            series4.Points.Add(dataPoint11);
            series4.Points.Add(dataPoint12);
            this.HistogramRed.Series.Add(series4);
            this.HistogramRed.Size = new System.Drawing.Size(308, 173);
            this.HistogramRed.TabIndex = 1;
            this.HistogramRed.Text = "chart1";
            title4.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            title4.Name = "Red";
            title4.Text = "Red";
            this.HistogramRed.Titles.Add(title4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageToolStripMenuItem,
            this.operationToolStripMenuItem,
            this.functionFiltersToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(977, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.getIrisToolStripMenuItem,
            this.kMMToolStripMenuItem,
            this.k3MToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.imageToolStripMenuItem.Text = "Image";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // getIrisToolStripMenuItem
            // 
            this.getIrisToolStripMenuItem.Name = "getIrisToolStripMenuItem";
            this.getIrisToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.getIrisToolStripMenuItem.Text = "Get Iris";
            this.getIrisToolStripMenuItem.Click += new System.EventHandler(this.getIrisToolStripMenuItem_Click);
            // 
            // operationToolStripMenuItem
            // 
            this.operationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeBrightnessToolStripMenuItem,
            this.changeContrastToolStripMenuItem,
            this.contrast2ToolStripMenuItem,
            this.changeToGrayscaleToolStripMenuItem,
            this.negationToolStripMenuItem,
            this.thresholdingToolStripMenuItem,
            this.histogramStretchingToolStripMenuItem,
            this.histogramEqualizationToolStripMenuItem,
            this.projectionsToolStripMenuItem});
            this.operationToolStripMenuItem.Name = "operationToolStripMenuItem";
            this.operationToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.operationToolStripMenuItem.Text = "Operations";
            // 
            // changeBrightnessToolStripMenuItem
            // 
            this.changeBrightnessToolStripMenuItem.Name = "changeBrightnessToolStripMenuItem";
            this.changeBrightnessToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.changeBrightnessToolStripMenuItem.Text = "Brightness";
            this.changeBrightnessToolStripMenuItem.Click += new System.EventHandler(this.changeBrightnessToolStripMenuItem_Click);
            // 
            // changeContrastToolStripMenuItem
            // 
            this.changeContrastToolStripMenuItem.Name = "changeContrastToolStripMenuItem";
            this.changeContrastToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.changeContrastToolStripMenuItem.Text = "Contrast";
            this.changeContrastToolStripMenuItem.Click += new System.EventHandler(this.changeContrastToolStripMenuItem_Click);
            // 
            // contrast2ToolStripMenuItem
            // 
            this.contrast2ToolStripMenuItem.Name = "contrast2ToolStripMenuItem";
            this.contrast2ToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.contrast2ToolStripMenuItem.Text = "Contrast2";
            this.contrast2ToolStripMenuItem.Click += new System.EventHandler(this.contrast2ToolStripMenuItem_Click);
            // 
            // changeToGrayscaleToolStripMenuItem
            // 
            this.changeToGrayscaleToolStripMenuItem.Name = "changeToGrayscaleToolStripMenuItem";
            this.changeToGrayscaleToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.changeToGrayscaleToolStripMenuItem.Text = "Grayscale";
            this.changeToGrayscaleToolStripMenuItem.Click += new System.EventHandler(this.changeToGrayscaleToolStripMenuItem_Click);
            // 
            // negationToolStripMenuItem
            // 
            this.negationToolStripMenuItem.Name = "negationToolStripMenuItem";
            this.negationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.negationToolStripMenuItem.Text = "Negation";
            this.negationToolStripMenuItem.Click += new System.EventHandler(this.negationToolStripMenuItem_Click);
            // 
            // thresholdingToolStripMenuItem
            // 
            this.thresholdingToolStripMenuItem.Name = "thresholdingToolStripMenuItem";
            this.thresholdingToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.thresholdingToolStripMenuItem.Text = "Thresholding";
            this.thresholdingToolStripMenuItem.Click += new System.EventHandler(this.thresholdingToolStripMenuItem_Click);
            // 
            // histogramStretchingToolStripMenuItem
            // 
            this.histogramStretchingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.HistogramStretchingRedToolStripMenuItem,
            this.HistogramStretchingGreenToolStripMenuItem,
            this.HistogramStretchingBlueToolStripMenuItem});
            this.histogramStretchingToolStripMenuItem.Name = "histogramStretchingToolStripMenuItem";
            this.histogramStretchingToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.histogramStretchingToolStripMenuItem.Text = "Histogram stretching";
            this.histogramStretchingToolStripMenuItem.Click += new System.EventHandler(this.histogramStretchingToolStripMenuItem_Click);
            // 
            // HistogramStretchingRedToolStripMenuItem
            // 
            this.HistogramStretchingRedToolStripMenuItem.Name = "HistogramStretchingRedToolStripMenuItem";
            this.HistogramStretchingRedToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.HistogramStretchingRedToolStripMenuItem.Text = "Red";
            this.HistogramStretchingRedToolStripMenuItem.Click += new System.EventHandler(this.HistogramStretchingRedToolStripMenuItem_Click);
            // 
            // HistogramStretchingGreenToolStripMenuItem
            // 
            this.HistogramStretchingGreenToolStripMenuItem.Name = "HistogramStretchingGreenToolStripMenuItem";
            this.HistogramStretchingGreenToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.HistogramStretchingGreenToolStripMenuItem.Text = "Green";
            this.HistogramStretchingGreenToolStripMenuItem.Click += new System.EventHandler(this.HistogramStretchingGreenToolStripMenuItem_Click);
            // 
            // HistogramStretchingBlueToolStripMenuItem
            // 
            this.HistogramStretchingBlueToolStripMenuItem.Name = "HistogramStretchingBlueToolStripMenuItem";
            this.HistogramStretchingBlueToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.HistogramStretchingBlueToolStripMenuItem.Text = "Blue";
            this.HistogramStretchingBlueToolStripMenuItem.Click += new System.EventHandler(this.HistogramStretchingBlueToolStripMenuItem_Click);
            // 
            // histogramEqualizationToolStripMenuItem
            // 
            this.histogramEqualizationToolStripMenuItem.Name = "histogramEqualizationToolStripMenuItem";
            this.histogramEqualizationToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.histogramEqualizationToolStripMenuItem.Text = "Histogram equalization";
            this.histogramEqualizationToolStripMenuItem.Click += new System.EventHandler(this.histogramEqualizationToolStripMenuItem_Click);
            // 
            // projectionsToolStripMenuItem
            // 
            this.projectionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontalProjectionToolStripMenuItem,
            this.verticalProjectionToolStripMenuItem});
            this.projectionsToolStripMenuItem.Name = "projectionsToolStripMenuItem";
            this.projectionsToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.projectionsToolStripMenuItem.Text = "Projections";
            // 
            // horizontalProjectionToolStripMenuItem
            // 
            this.horizontalProjectionToolStripMenuItem.Name = "horizontalProjectionToolStripMenuItem";
            this.horizontalProjectionToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.horizontalProjectionToolStripMenuItem.Text = "Horizontal Projection";
            this.horizontalProjectionToolStripMenuItem.Click += new System.EventHandler(this.horizontalProjectionToolStripMenuItem_Click);
            // 
            // verticalProjectionToolStripMenuItem
            // 
            this.verticalProjectionToolStripMenuItem.Name = "verticalProjectionToolStripMenuItem";
            this.verticalProjectionToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.verticalProjectionToolStripMenuItem.Text = "Vertical Projection";
            this.verticalProjectionToolStripMenuItem.Click += new System.EventHandler(this.verticalProjectionToolStripMenuItem_Click);
            // 
            // functionFiltersToolStripMenuItem
            // 
            this.functionFiltersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lowPassToolStripMenuItem,
            this.gaussianToolStripMenuItem,
            this.sharpeningToolStripMenuItem,
            this.edgeDetectionToolStripMenuItem,
            this.sculpingToolStripMenuItem});
            this.functionFiltersToolStripMenuItem.Name = "functionFiltersToolStripMenuItem";
            this.functionFiltersToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.functionFiltersToolStripMenuItem.Text = "Matrix Filters";
            // 
            // lowPassToolStripMenuItem
            // 
            this.lowPassToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lowPass1ToolStripMenuItem,
            this.lowPass2ToolStripMenuItem,
            this.lowPass3ToolStripMenuItem,
            this.lowPass4ToolStripMenuItem});
            this.lowPassToolStripMenuItem.Name = "lowPassToolStripMenuItem";
            this.lowPassToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.lowPassToolStripMenuItem.Text = "LowPass";
            // 
            // lowPass1ToolStripMenuItem
            // 
            this.lowPass1ToolStripMenuItem.Name = "lowPass1ToolStripMenuItem";
            this.lowPass1ToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.lowPass1ToolStripMenuItem.Text = "LowPass1";
            this.lowPass1ToolStripMenuItem.Click += new System.EventHandler(this.lowPass1ToolStripMenuItem_Click);
            // 
            // lowPass2ToolStripMenuItem
            // 
            this.lowPass2ToolStripMenuItem.Name = "lowPass2ToolStripMenuItem";
            this.lowPass2ToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.lowPass2ToolStripMenuItem.Text = "LowPass2";
            this.lowPass2ToolStripMenuItem.Click += new System.EventHandler(this.lowPass2ToolStripMenuItem_Click);
            // 
            // lowPass3ToolStripMenuItem
            // 
            this.lowPass3ToolStripMenuItem.Name = "lowPass3ToolStripMenuItem";
            this.lowPass3ToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.lowPass3ToolStripMenuItem.Text = "LowPass3";
            this.lowPass3ToolStripMenuItem.Click += new System.EventHandler(this.lowPass3ToolStripMenuItem_Click);
            // 
            // lowPass4ToolStripMenuItem
            // 
            this.lowPass4ToolStripMenuItem.Name = "lowPass4ToolStripMenuItem";
            this.lowPass4ToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.lowPass4ToolStripMenuItem.Text = "LowPass4";
            this.lowPass4ToolStripMenuItem.Click += new System.EventHandler(this.lowPass4ToolStripMenuItem_Click);
            // 
            // gaussianToolStripMenuItem
            // 
            this.gaussianToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gaussianBlur1ToolStripMenuItem,
            this.gaussianBlur2ToolStripMenuItem,
            this.gaussianBlur3ToolStripMenuItem,
            this.gaussianBlur4ToolStripMenuItem});
            this.gaussianToolStripMenuItem.Name = "gaussianToolStripMenuItem";
            this.gaussianToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.gaussianToolStripMenuItem.Text = "LowPassGaussian";
            // 
            // gaussianBlur1ToolStripMenuItem
            // 
            this.gaussianBlur1ToolStripMenuItem.Name = "gaussianBlur1ToolStripMenuItem";
            this.gaussianBlur1ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.gaussianBlur1ToolStripMenuItem.Text = "GaussianBlur1";
            this.gaussianBlur1ToolStripMenuItem.Click += new System.EventHandler(this.gaussianBlur1ToolStripMenuItem_Click);
            // 
            // gaussianBlur2ToolStripMenuItem
            // 
            this.gaussianBlur2ToolStripMenuItem.Name = "gaussianBlur2ToolStripMenuItem";
            this.gaussianBlur2ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.gaussianBlur2ToolStripMenuItem.Text = "GaussianBlur2";
            this.gaussianBlur2ToolStripMenuItem.Click += new System.EventHandler(this.gaussianBlur2ToolStripMenuItem_Click);
            // 
            // gaussianBlur3ToolStripMenuItem
            // 
            this.gaussianBlur3ToolStripMenuItem.Name = "gaussianBlur3ToolStripMenuItem";
            this.gaussianBlur3ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.gaussianBlur3ToolStripMenuItem.Text = "GaussianBlur3";
            this.gaussianBlur3ToolStripMenuItem.Click += new System.EventHandler(this.gaussianBlur3ToolStripMenuItem_Click);
            // 
            // gaussianBlur4ToolStripMenuItem
            // 
            this.gaussianBlur4ToolStripMenuItem.Name = "gaussianBlur4ToolStripMenuItem";
            this.gaussianBlur4ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.gaussianBlur4ToolStripMenuItem.Text = "GaussianBlur4";
            this.gaussianBlur4ToolStripMenuItem.Click += new System.EventHandler(this.gaussianBlur4ToolStripMenuItem_Click);
            // 
            // sharpeningToolStripMenuItem
            // 
            this.sharpeningToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hPToolStripMenuItem,
            this.meanRemovalToolStripMenuItem});
            this.sharpeningToolStripMenuItem.Name = "sharpeningToolStripMenuItem";
            this.sharpeningToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.sharpeningToolStripMenuItem.Text = "HighPass";
            // 
            // hPToolStripMenuItem
            // 
            this.hPToolStripMenuItem.Name = "hPToolStripMenuItem";
            this.hPToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.hPToolStripMenuItem.Text = "HP";
            this.hPToolStripMenuItem.Click += new System.EventHandler(this.hPToolStripMenuItem_Click);
            // 
            // meanRemovalToolStripMenuItem
            // 
            this.meanRemovalToolStripMenuItem.Name = "meanRemovalToolStripMenuItem";
            this.meanRemovalToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.meanRemovalToolStripMenuItem.Text = "Mean Removal";
            this.meanRemovalToolStripMenuItem.Click += new System.EventHandler(this.meanRemovalToolStripMenuItem_Click);
            // 
            // edgeDetectionToolStripMenuItem
            // 
            this.edgeDetectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontalToolStripMenuItem,
            this.verticalToolStripMenuItem,
            this.diagonalToolStripMenuItem,
            this.laplace1ToolStripMenuItem,
            this.laplace2ToolStripMenuItem,
            this.robertsCrossToolStripMenuItem,
            this.robertsCross2ToolStripMenuItem});
            this.edgeDetectionToolStripMenuItem.Name = "edgeDetectionToolStripMenuItem";
            this.edgeDetectionToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.edgeDetectionToolStripMenuItem.Text = "Edge detection";
            // 
            // horizontalToolStripMenuItem
            // 
            this.horizontalToolStripMenuItem.Name = "horizontalToolStripMenuItem";
            this.horizontalToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.horizontalToolStripMenuItem.Text = "Horizontal";
            this.horizontalToolStripMenuItem.Click += new System.EventHandler(this.horizontalToolStripMenuItem_Click);
            // 
            // verticalToolStripMenuItem
            // 
            this.verticalToolStripMenuItem.Name = "verticalToolStripMenuItem";
            this.verticalToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.verticalToolStripMenuItem.Text = "Vertical";
            this.verticalToolStripMenuItem.Click += new System.EventHandler(this.verticalToolStripMenuItem_Click);
            // 
            // diagonalToolStripMenuItem
            // 
            this.diagonalToolStripMenuItem.Name = "diagonalToolStripMenuItem";
            this.diagonalToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.diagonalToolStripMenuItem.Text = "Diagonal";
            this.diagonalToolStripMenuItem.Click += new System.EventHandler(this.diagonalToolStripMenuItem_Click);
            // 
            // laplace1ToolStripMenuItem
            // 
            this.laplace1ToolStripMenuItem.Name = "laplace1ToolStripMenuItem";
            this.laplace1ToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.laplace1ToolStripMenuItem.Text = "Laplace1";
            this.laplace1ToolStripMenuItem.Click += new System.EventHandler(this.laplace1ToolStripMenuItem_Click);
            // 
            // laplace2ToolStripMenuItem
            // 
            this.laplace2ToolStripMenuItem.Name = "laplace2ToolStripMenuItem";
            this.laplace2ToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.laplace2ToolStripMenuItem.Text = "Laplace2";
            this.laplace2ToolStripMenuItem.Click += new System.EventHandler(this.laplace2ToolStripMenuItem_Click);
            // 
            // robertsCrossToolStripMenuItem
            // 
            this.robertsCrossToolStripMenuItem.Name = "robertsCrossToolStripMenuItem";
            this.robertsCrossToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.robertsCrossToolStripMenuItem.Text = "Robert\'s Cross";
            this.robertsCrossToolStripMenuItem.Click += new System.EventHandler(this.robertsCrossToolStripMenuItem_Click);
            // 
            // robertsCross2ToolStripMenuItem
            // 
            this.robertsCross2ToolStripMenuItem.Name = "robertsCross2ToolStripMenuItem";
            this.robertsCross2ToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.robertsCross2ToolStripMenuItem.Text = "Robert\'s Cross2";
            this.robertsCross2ToolStripMenuItem.Click += new System.EventHandler(this.robertsCross2ToolStripMenuItem_Click);
            // 
            // sculpingToolStripMenuItem
            // 
            this.sculpingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sculpingEToolStripMenuItem,
            this.sculpingSToolStripMenuItem,
            this.sculpingSE1ToolStripMenuItem,
            this.sculpingSE2ToolStripMenuItem});
            this.sculpingToolStripMenuItem.Name = "sculpingToolStripMenuItem";
            this.sculpingToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.sculpingToolStripMenuItem.Text = "Sculpting";
            // 
            // sculpingEToolStripMenuItem
            // 
            this.sculpingEToolStripMenuItem.Name = "sculpingEToolStripMenuItem";
            this.sculpingEToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.sculpingEToolStripMenuItem.Text = "SculptingE";
            this.sculpingEToolStripMenuItem.Click += new System.EventHandler(this.sculpingEToolStripMenuItem_Click);
            // 
            // sculpingSToolStripMenuItem
            // 
            this.sculpingSToolStripMenuItem.Name = "sculpingSToolStripMenuItem";
            this.sculpingSToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.sculpingSToolStripMenuItem.Text = "SculptingS";
            this.sculpingSToolStripMenuItem.Click += new System.EventHandler(this.sculpingSToolStripMenuItem_Click);
            // 
            // sculpingSE1ToolStripMenuItem
            // 
            this.sculpingSE1ToolStripMenuItem.Name = "sculpingSE1ToolStripMenuItem";
            this.sculpingSE1ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.sculpingSE1ToolStripMenuItem.Text = "SculptingSE1";
            this.sculpingSE1ToolStripMenuItem.Click += new System.EventHandler(this.sculpingSE1ToolStripMenuItem_Click);
            // 
            // sculpingSE2ToolStripMenuItem
            // 
            this.sculpingSE2ToolStripMenuItem.Name = "sculpingSE2ToolStripMenuItem";
            this.sculpingSE2ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.sculpingSE2ToolStripMenuItem.Text = "SculptingSE2";
            this.sculpingSE2ToolStripMenuItem.Click += new System.EventHandler(this.sculpingSE2ToolStripMenuItem_Click);
            // 
            // HistogramGreen
            // 
            chartArea5.Name = "ChartArea1";
            this.HistogramGreen.ChartAreas.Add(chartArea5);
            this.HistogramGreen.Dock = System.Windows.Forms.DockStyle.Bottom;
            legend5.Enabled = false;
            legend5.Name = "Legend1";
            this.HistogramGreen.Legends.Add(legend5);
            this.HistogramGreen.Location = new System.Drawing.Point(0, 373);
            this.HistogramGreen.Name = "HistogramGreen";
            series5.ChartArea = "ChartArea1";
            series5.Color = System.Drawing.Color.Green;
            series5.Legend = "Legend1";
            series5.Name = "GreenSeries";
            series5.Points.Add(dataPoint13);
            series5.Points.Add(dataPoint14);
            series5.Points.Add(dataPoint15);
            this.HistogramGreen.Series.Add(series5);
            this.HistogramGreen.Size = new System.Drawing.Size(308, 173);
            this.HistogramGreen.TabIndex = 3;
            this.HistogramGreen.Text = "chart1";
            title5.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            title5.Name = "Green";
            title5.Text = "Green";
            this.HistogramGreen.Titles.Add(title5);
            // 
            // HistogramBlue
            // 
            chartArea6.Name = "ChartArea1";
            this.HistogramBlue.ChartAreas.Add(chartArea6);
            this.HistogramBlue.Dock = System.Windows.Forms.DockStyle.Bottom;
            legend6.Enabled = false;
            legend6.Name = "Legend1";
            this.HistogramBlue.Legends.Add(legend6);
            this.HistogramBlue.Location = new System.Drawing.Point(0, 200);
            this.HistogramBlue.Name = "HistogramBlue";
            series6.ChartArea = "ChartArea1";
            series6.Color = System.Drawing.Color.Blue;
            series6.Legend = "Legend1";
            series6.Name = "BlueSeries";
            series6.Points.Add(dataPoint16);
            series6.Points.Add(dataPoint17);
            series6.Points.Add(dataPoint18);
            this.HistogramBlue.Series.Add(series6);
            this.HistogramBlue.Size = new System.Drawing.Size(308, 173);
            this.HistogramBlue.TabIndex = 4;
            this.HistogramBlue.Text = "chart1";
            title6.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            title6.Name = "Blue";
            title6.Text = "Blue";
            this.HistogramBlue.Titles.Add(title6);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Picture);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(977, 546);
            this.splitContainer1.SplitterDistance = 665;
            this.splitContainer1.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.HistogramRed);
            this.panel1.Controls.Add(this.HistogramBlue);
            this.panel1.Controls.Add(this.HistogramGreen);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(308, 546);
            this.panel1.TabIndex = 6;
            // 
            // kMMToolStripMenuItem
            // 
            this.kMMToolStripMenuItem.Name = "kMMToolStripMenuItem";
            this.kMMToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.kMMToolStripMenuItem.Text = "KMM";
            this.kMMToolStripMenuItem.Click += new System.EventHandler(this.kMMToolStripMenuItem_Click);
            // 
            // k3MToolStripMenuItem
            // 
            this.k3MToolStripMenuItem.Name = "k3MToolStripMenuItem";
            this.k3MToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.k3MToolStripMenuItem.Text = "K3M";
            this.k3MToolStripMenuItem.Click += new System.EventHandler(this.k3MToolStripMenuItem_Click);
            // 
            // AppForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 546);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AppForm";
            this.Text = "Image Editor";
            ((System.ComponentModel.ISupportInitialize)(this.Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistogramRed)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HistogramGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HistogramBlue)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Picture;
        private System.Windows.Forms.DataVisualization.Charting.Chart HistogramRed;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem operationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeBrightnessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeContrastToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart HistogramGreen;
        private System.Windows.Forms.DataVisualization.Charting.Chart HistogramBlue;
        private System.Windows.Forms.ToolStripMenuItem changeToGrayscaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thresholdingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem histogramStretchingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem histogramEqualizationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem functionFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gaussianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharpeningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgeDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HistogramStretchingRedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HistogramStretchingGreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HistogramStretchingBlueToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem gaussianBlur1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gaussianBlur2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem meanRemovalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagonalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laplace1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laplace2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowPassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowPass1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowPass2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowPass3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lowPass4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem robertsCrossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem robertsCross2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalProjectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticalProjectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contrast2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gaussianBlur3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gaussianBlur4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sculpingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sculpingEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sculpingSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sculpingSE1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sculpingSE2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem getIrisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kMMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem k3MToolStripMenuItem;
    }
}

